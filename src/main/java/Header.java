import java.util.List;

public class Header
{
    public final Token argument;
    public final Token name;
    public final Token returnType;

    public Header(final String _argument, final String _name, final String _returnType)
    {
        argument = new Token(_argument);
        name = new Token(_name);
        returnType = new Token(_returnType);
    }

    public Header(final Token _argument, final Token _name, final Token _returnType)
    {
        argument = _argument;
        name = _name;
        returnType = _returnType;
    }

    public Header(final String _line, final List<Token> _tokens)
    {
        switch (_tokens.size())
        {
            case 1:
                argument = DefaultTypes.ANY_TOKEN;
                name = _tokens.get(0);
                returnType = _tokens.get(0);
                break;

            case 2:
                argument = DefaultTypes.ANY_TOKEN;
                name = _tokens.get(0);
                returnType = _tokens.get(1);
                break;

            case 3:
                argument = _tokens.get(0);
                name = _tokens.get(1);
                returnType = _tokens.get(2);
                break;

            default:
                throw new RuntimeException("unknown header: " + _line);
        }
    }
}
