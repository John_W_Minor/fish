import java.util.Deque;

public class Word
{
    public final Token name;
    public final Token returnType;
    public final Token argument;

    private final Word[] definition;

    private final TerminalWordComputer computer;

    private final boolean terminal;

    public Word(final Header _header, final Word[] _definition)
    {
        name = _header.name;
        returnType = _header.returnType;
        argument = _header.argument;

        definition = _definition;

        computer = null;

        terminal = false;

        Word previousWord = null;
        Token previousReturnType = null;
        for(Word word : definition)
        {
            if(previousWord == null)
            {
                previousWord = word;
                previousReturnType = word.returnType;
                continue;
            }

            if(!previousReturnType.equals(DefaultTypes.ANY_TOKEN) && !word.argument.equals(DefaultTypes.ANY_TOKEN) && !previousReturnType.equals(word.argument))
            {
                throw new RuntimeException("typing error for words: " + previousWord + " and " + word);
            }

            Token currentReturnType = word.returnType;
            if(!currentReturnType.equals(DefaultTypes.ANY_TOKEN))
            {
                previousWord = word;
                previousReturnType = currentReturnType;
            }
        }
    }

    public Word(final Header _header, final TerminalWordComputer _computer)
    {
        name = _header.name;
        returnType = _header.returnType;
        argument = _header.argument;

        definition = null;

        computer = _computer;

        terminal = true;
    }

    public StackFrame compute(final Deque<Integer> _deque, Integer _a, Integer _b, Integer _c, Integer _d)
    {
        if(terminal)
        {
            return computer.compute(_deque, _a, _b, _c, _d);
        }

        for(int pc = 0; pc < definition.length; pc++)
        {
            Word word = definition[pc];

            StackFrame stackFrame = word.compute(_deque, _a, _b, _c, _d);

            switch (stackFrame.stackFrameCommand)
            {
                case STORE_A:
                    _a = _deque.pop();
                    break;
    
                case STORE_B:
                    _b = _deque.pop();
                    break;
    
                case STORE_C:
                    _c = _deque.pop();
                    break;
    
                case STORE_D:
                    _d = _deque.pop();
                    break;
                
                case BREAK:
                    pc = Integer.MAX_VALUE - 1;
                    break;
                
                case LOOP:
                    pc = -1;
                    break;

                case END:
                    return new StackFrame(StackFrameCommand.END, _a, _b, _c, _d);
            }
        }

        return new StackFrame(StackFrameCommand.CONTINUE, _a, _b, _c, _d);
    }

    @Override
    public String toString()
    {
        return name.toString();
    }

    @FunctionalInterface
    public interface TerminalWordComputer
    {
        StackFrame compute(final Deque<Integer> _deque, Integer _a, Integer _b, Integer _c, Integer _d);
    }

    public static class StackFrame
    {
        public final StackFrameCommand stackFrameCommand;
        public final Integer a;
        public final Integer b;
        public final Integer c;
        public final Integer d;

        public StackFrame(final StackFrameCommand _stackFrameCommand, final Integer _a, final Integer _b, final Integer _c, final Integer _d)
        {
            stackFrameCommand = _stackFrameCommand;
            a = _a;
            b = _b;
            c = _c;
            d = _d;
        }
    }

    enum StackFrameCommand
    {
        STORE_A,
        STORE_B,
        STORE_C,
        STORE_D,
        BREAK,
        CONTINUE,
        LOOP,
        END
    }
}
