import java.io.FileReader;
import java.util.LinkedList;
import java.util.Scanner;

public class Main
{
    public static void main(String[] _args) throws Exception
    {
        String path = "src\\main\\resources\\source.fish";
        StringBuilder builder = new StringBuilder();
        try(Scanner scanner = new Scanner(new FileReader(path)))
        {
            while(scanner.hasNext())
            {
                builder.append(scanner.nextLine());
                if(scanner.hasNext())
                {
                    builder.append("\n");
                }
            }
        }

        String in = builder.toString();

        Word word = Compiler.compile(in);

        word.compute(new LinkedList<>(), 0,0,0,0);
    }
}
