import com.sun.org.apache.regexp.internal.RE;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Compiler
{
    public static final InputStreamReader STREAM = new InputStreamReader(System.in);
    public static final BufferedReader READER = new BufferedReader(STREAM);
    
    public static Word compile(final String _source)
    {
        final Map<Token, Word> dictionary = new HashMap<>();
        populateDefaultWords(dictionary);
        
        Word main = null;
        
        for (String line : toLines(_source))
        {
            final String[] headerAndDefinition = splitHeaderFromDefinition(line);
            
            final String headerString = headerAndDefinition[0];
            final Header header = new Header(headerString, tokenize(headerString));
            
            final String definition = headerAndDefinition[1];
            
            final Word word = buildWord(header, definition, dictionary);
            
            final Token wordName = word.name;
            
            if (wordName.type != Token.TokenType.ANY)
            {
                throw new RuntimeException("invalid word: " + wordName);
            }
            
            if (dictionary.containsKey(wordName))
            {
                throw new RuntimeException("duplicate word: " + wordName);
            }
            
            if (wordName.name.equals("main"))
            {
                main = word;
            }
            
            dictionary.put(wordName, word);
        }
        
        return main;
    }
    
    private static void addDefaultWord(final String _argument, final String _name, final String _returnType, final Map<Token, Word> _dictionary, final Word.TerminalWordComputer _computer)
    {
        final Header header = new Header(_argument, _name, _returnType);
        final Word word = new Word(header, _computer);
        _dictionary.put(header.name, word);
    }
    
    private static void populateDefaultWords(final Map<Token, Word> _dictionary)
    {
        //io
        {
            addDefaultWord(DefaultTypes.ANY, "io", DefaultTypes.INT, _dictionary, (_deque, _a, _b, _c, _d) ->
            {
                Integer a = null;
                
                while(a == null)
                {
                    try
                    {
                        a = Integer.parseInt(READER.readLine());
                    } catch (Exception e) {}
                }
                
                _deque.push(a);
                
                return new Word.StackFrame(Word.StackFrameCommand.CONTINUE, _a, _b, _c, _d);
            });
        }
        
        //addition
        {
            addDefaultWord(DefaultTypes.INT, "+", DefaultTypes.INT, _dictionary, (_deque, _a, _b, _c, _d) ->
            {
                final int a = _deque.pop();
                final int b = _deque.pop();
                
                _deque.push(b + a);
                
                return new Word.StackFrame(Word.StackFrameCommand.CONTINUE, _a, _b, _c, _d);
            });
        }
        
        //subtraction
        {
            addDefaultWord(DefaultTypes.INT, "-", DefaultTypes.INT, _dictionary, (_deque, _a, _b, _c, _d) ->
            {
                final int a = _deque.pop();
                final int b = _deque.pop();
                
                _deque.push(b - a);
                
                return new Word.StackFrame(Word.StackFrameCommand.CONTINUE, _a, _b, _c, _d);
            });
        }
        
        //multiplication
        {
            addDefaultWord(DefaultTypes.INT, "*", DefaultTypes.INT, _dictionary, (_deque, _a, _b, _c, _d) ->
            {
                final int a = _deque.pop();
                final int b = _deque.pop();
                
                _deque.push(b * a);
                
                return new Word.StackFrame(Word.StackFrameCommand.CONTINUE, _a, _b, _c, _d);
            });
        }
        
        //division
        {
            addDefaultWord(DefaultTypes.INT, "/", DefaultTypes.INT, _dictionary, (_deque, _a, _b, _c, _d) ->
            {
                final int a = _deque.pop();
                final int b = _deque.pop();
                
                _deque.push(b / a);
                
                return new Word.StackFrame(Word.StackFrameCommand.CONTINUE, _a, _b, _c, _d);
            });
        }
        
        //mod
        {
            addDefaultWord(DefaultTypes.INT, "%", DefaultTypes.INT, _dictionary, (_deque, _a, _b, _c, _d) ->
            {
                final int a = _deque.pop();
                final int b = _deque.pop();
                
                _deque.push(b % a);
                
                return new Word.StackFrame(Word.StackFrameCommand.CONTINUE, _a, _b, _c, _d);
            });
        }
        
        //pop
        {
            addDefaultWord(DefaultTypes.ANY, "pop", DefaultTypes.UNKNOWN, _dictionary, (_deque, _a, _b, _c, _d) ->
            {
                _deque.pop();
                
                return new Word.StackFrame(Word.StackFrameCommand.CONTINUE, _a, _b, _c, _d);
            });
        }
        
        //duplicate
        {
            addDefaultWord(DefaultTypes.INT, "duplicate", DefaultTypes.UNKNOWN, _dictionary, (_deque, _a, _b, _c, _d) ->
            {
                final int a = _deque.pop();
                
                Deque<Integer> b = new LinkedList<>();
                Deque<Integer> c = new LinkedList<>();
                
                for (int i = 0; i < a; i++)
                {
                    final int d = _deque.pop();
                    b.push(d);
                    c.push(d);
                }
                
                while (!b.isEmpty())
                {
                    _deque.push(b.pop());
                }
                
                while (!c.isEmpty())
                {
                    _deque.push(c.pop());
                }
                
                return new Word.StackFrame(Word.StackFrameCommand.CONTINUE, _a, _b, _c, _d);
            });
        }
        
        //swap
        {
            addDefaultWord(DefaultTypes.ANY, "swap", DefaultTypes.UNKNOWN, _dictionary, (_deque, _a, _b, _c, _d) ->
            {
                final int a = _deque.pop();
                final int b = _deque.pop();
                
                _deque.push(a);
                _deque.push(b);
                
                return new Word.StackFrame(Word.StackFrameCommand.CONTINUE, _a, _b, _c, _d);
            });
        }
        
        //set_value
        {
            addDefaultWord(DefaultTypes.INT, "set_value", DefaultTypes.UNKNOWN, _dictionary, (_deque, _a, _b, _c, _d) ->
            {
                final int a = _deque.pop();
                final int b = _deque.pop();
                
                Deque<Integer> c = new LinkedList<>();
                
                for (int i = 0; i < a; i++)
                {
                    final int d = _deque.pop();
                    c.push(d);
                }
                
                c.pop();
                c.push(b);
                
                while (!c.isEmpty())
                {
                    _deque.push(c.pop());
                }
                
                return new Word.StackFrame(Word.StackFrameCommand.CONTINUE, _a, _b, _c, _d);
            });
        }
        
        //get_value
        {
            addDefaultWord(DefaultTypes.INT, "get_value", DefaultTypes.UNKNOWN, _dictionary, (_deque, _a, _b, _c, _d) ->
            {
                final int a = _deque.pop();
                
                Deque<Integer> b = new LinkedList<>();
                
                for (int i = 0; i < a; i++)
                {
                    final int d = _deque.pop();
                    b.push(d);
                }
                
                final int c = b.peek();
                
                while (!b.isEmpty())
                {
                    _deque.push(b.pop());
                }
                
                _deque.push(c);
                
                return new Word.StackFrame(Word.StackFrameCommand.CONTINUE, _a, _b, _c, _d);
            });
        }
        
        //store_a
        {
            addDefaultWord(DefaultTypes.ANY, "store_a", DefaultTypes.STORE_A, _dictionary, (_deque, _a, _b, _c, _d) -> new Word.StackFrame(Word.StackFrameCommand.STORE_A, _a, _b, _c, _d));
        }
        
        //put_a
        {
            addDefaultWord(DefaultTypes.ANY, "put_a", DefaultTypes.UNKNOWN, _dictionary, (_deque, _a, _b, _c, _d) ->
            {
                _deque.push(_a);
                
                return new Word.StackFrame(Word.StackFrameCommand.CONTINUE, _a, _b, _c, _d);
            });
        }
        
        //store_b
        {
            addDefaultWord(DefaultTypes.ANY, "store_b", DefaultTypes.STORE_B, _dictionary, (_deque, _a, _b, _c, _d) -> new Word.StackFrame(Word.StackFrameCommand.STORE_B, _a, _b, _c, _d));
        }
        
        //put_b
        {
            addDefaultWord(DefaultTypes.ANY, "put_b", DefaultTypes.UNKNOWN, _dictionary, (_deque, _a, _b, _c, _d) ->
            {
                _deque.push(_b);
                
                return new Word.StackFrame(Word.StackFrameCommand.CONTINUE, _a, _b, _c, _d);
            });
        }
        
        //store_c
        {
            addDefaultWord(DefaultTypes.ANY, "store_c", DefaultTypes.STORE_C, _dictionary, (_deque, _a, _b, _c, _d) -> new Word.StackFrame(Word.StackFrameCommand.STORE_C, _a, _b, _c, _d));
        }
        
        //put_c
        {
            addDefaultWord(DefaultTypes.ANY, "put_c", DefaultTypes.UNKNOWN, _dictionary, (_deque, _a, _b, _c, _d) ->
            {
                _deque.push(_c);
                
                return new Word.StackFrame(Word.StackFrameCommand.CONTINUE, _a, _b, _c, _d);
            });
        }
        
        //store_d
        {
            addDefaultWord(DefaultTypes.ANY, "store_d", DefaultTypes.STORE_D, _dictionary, (_deque, _a, _b, _c, _d) -> new Word.StackFrame(Word.StackFrameCommand.STORE_D, _a, _b, _c, _d));
        }
        
        //put_d
        {
            addDefaultWord(DefaultTypes.ANY, "put_d", DefaultTypes.UNKNOWN, _dictionary, (_deque, _a, _b, _c, _d) ->
            {
                _deque.push(_d);
                
                return new Word.StackFrame(Word.StackFrameCommand.CONTINUE, _a, _b, _c, _d);
            });
        }
        
        //true
        {
            addDefaultWord(DefaultTypes.ANY, "true", DefaultTypes.BOOL, _dictionary, (_deque, _a, _b, _c, _d) ->
            {
                _deque.push(DefaultTypes.TRUE);
                
                return new Word.StackFrame(Word.StackFrameCommand.CONTINUE, _a, _b, _c, _d);
            });
        }
        
        //false
        {
            addDefaultWord(DefaultTypes.ANY, "false", DefaultTypes.BOOL, _dictionary, (_deque, _a, _b, _c, _d) ->
            {
                _deque.push(DefaultTypes.FALSE);
                
                return new Word.StackFrame(Word.StackFrameCommand.CONTINUE, _a, _b, _c, _d);
            });
        }
        
        //true_break
        {
            addDefaultWord(DefaultTypes.BOOL, "if_false", DefaultTypes.UNKNOWN, _dictionary, (_deque, _a, _b, _c, _d) ->
            {
                final int a = _deque.pop();
                
                if (a != 0)
                {
                    return new Word.StackFrame(Word.StackFrameCommand.BREAK, _a, _b, _c, _d);
                }
                
                return new Word.StackFrame(Word.StackFrameCommand.CONTINUE, _a, _b, _c, _d);
            });
        }
        
        //false_break
        {
            addDefaultWord(DefaultTypes.BOOL, "if_true", DefaultTypes.UNKNOWN, _dictionary, (_deque, _a, _b, _c, _d) ->
            {
                final int a = _deque.pop();
                
                if (a == 0)
                {
                    return new Word.StackFrame(Word.StackFrameCommand.BREAK, _a, _b, _c, _d);
                }
                
                return new Word.StackFrame(Word.StackFrameCommand.CONTINUE, _a, _b, _c, _d);
            });
        }
        
        //==
        {
            addDefaultWord(DefaultTypes.INT, "==", DefaultTypes.BOOL, _dictionary, (_deque, _a, _b, _c, _d) ->
            {
                final int a = _deque.pop();
                final int b = _deque.pop();
                
                _deque.push(b == a ? DefaultTypes.TRUE : DefaultTypes.FALSE);
                
                return new Word.StackFrame(Word.StackFrameCommand.CONTINUE, _a, _b, _c, _d);
            });
        }
        
        //!=
        {
            addDefaultWord(DefaultTypes.INT, "!=", DefaultTypes.BOOL, _dictionary, (_deque, _a, _b, _c, _d) ->
            {
                final int a = _deque.pop();
                final int b = _deque.pop();
                
                _deque.push(b != a ? DefaultTypes.TRUE : DefaultTypes.FALSE);
                
                return new Word.StackFrame(Word.StackFrameCommand.CONTINUE, _a, _b, _c, _d);
            });
        }
        
        //>=
        {
            addDefaultWord(DefaultTypes.INT, ">=", DefaultTypes.BOOL, _dictionary, (_deque, _a, _b, _c, _d) ->
            {
                final int a = _deque.pop();
                final int b = _deque.pop();
                
                _deque.push(b >= a ? DefaultTypes.TRUE : DefaultTypes.FALSE);
                
                return new Word.StackFrame(Word.StackFrameCommand.CONTINUE, _a, _b, _c, _d);
            });
        }
        
        //<=
        {
            addDefaultWord(DefaultTypes.INT, "<=", DefaultTypes.BOOL, _dictionary, (_deque, _a, _b, _c, _d) ->
            {
                final int a = _deque.pop();
                final int b = _deque.pop();
                
                _deque.push(b <= a ? DefaultTypes.TRUE : DefaultTypes.FALSE);
                
                return new Word.StackFrame(Word.StackFrameCommand.CONTINUE, _a, _b, _c, _d);
            });
        }
        
        //>
        {
            addDefaultWord(DefaultTypes.INT, ">", DefaultTypes.BOOL, _dictionary, (_deque, _a, _b, _c, _d) ->
            {
                final int a = _deque.pop();
                final int b = _deque.pop();
                
                _deque.push(b > a ? DefaultTypes.TRUE : DefaultTypes.FALSE);
                
                return new Word.StackFrame(Word.StackFrameCommand.CONTINUE, _a, _b, _c, _d);
            });
        }
        
        //<
        {
            addDefaultWord(DefaultTypes.INT, "<", DefaultTypes.BOOL, _dictionary, (_deque, _a, _b, _c, _d) ->
            {
                final int a = _deque.pop();
                final int b = _deque.pop();
                
                _deque.push(b < a ? DefaultTypes.TRUE : DefaultTypes.FALSE);
                
                return new Word.StackFrame(Word.StackFrameCommand.CONTINUE, _a, _b, _c, _d);
            });
        }
        
        //logical &&
        {
            addDefaultWord(DefaultTypes.BOOL, "&&", DefaultTypes.BOOL, _dictionary, (_deque, _a, _b, _c, _d) ->
            {
                final int a = _deque.pop();
                final int b = _deque.pop();
                
                _deque.push(b & a);
                
                return new Word.StackFrame(Word.StackFrameCommand.CONTINUE, _a, _b, _c, _d);
            });
        }
        
        //logical ||
        {
            addDefaultWord(DefaultTypes.BOOL, "||", DefaultTypes.BOOL, _dictionary, (_deque, _a, _b, _c, _d) ->
            {
                final int a = _deque.pop();
                final int b = _deque.pop();
                
                _deque.push(b | a);
                
                return new Word.StackFrame(Word.StackFrameCommand.CONTINUE, _a, _b, _c, _d);
            });
        }
        
        //bitwise &
        {
            addDefaultWord(DefaultTypes.INT, "&", DefaultTypes.INT, _dictionary, (_deque, _a, _b, _c, _d) ->
            {
                final int a = _deque.pop();
                final int b = _deque.pop();
                
                _deque.push(b & a);
                
                return new Word.StackFrame(Word.StackFrameCommand.CONTINUE, _a, _b, _c, _d);
            });
        }
        
        //bitwise ^
        {
            addDefaultWord(DefaultTypes.INT, "^", DefaultTypes.INT, _dictionary, (_deque, _a, _b, _c, _d) ->
            {
                final int a = _deque.pop();
                final int b = _deque.pop();
                
                _deque.push(b ^ a);
                
                return new Word.StackFrame(Word.StackFrameCommand.CONTINUE, _a, _b, _c, _d);
            });
        }
        
        //bitwise |
        {
            addDefaultWord(DefaultTypes.INT, "|", DefaultTypes.INT, _dictionary, (_deque, _a, _b, _c, _d) ->
            {
                final int a = _deque.pop();
                final int b = _deque.pop();
                
                _deque.push(b | a);
                
                return new Word.StackFrame(Word.StackFrameCommand.CONTINUE, _a, _b, _c, _d);
            });
        }
        
        //loop
        {
            addDefaultWord(DefaultTypes.ANY, "loop", DefaultTypes.UNKNOWN, _dictionary, (_deque, _a, _b, _c, _d) -> new Word.StackFrame(Word.StackFrameCommand.LOOP, _a, _b, _c, _d));
        }
        
        //end
        {
            addDefaultWord(DefaultTypes.ANY, "end", DefaultTypes.UNKNOWN, _dictionary, (_deque, _a, _b, _c, _d) -> new Word.StackFrame(Word.StackFrameCommand.END, _a, _b, _c, _d));
        }
        
        //print
        {
            addDefaultWord(DefaultTypes.ANY, "print", DefaultTypes.ANY, _dictionary, (_deque, _a, _b, _c, _d) ->
            {
                final int a = _deque.peek();
                
                System.out.print(a);
                
                return new Word.StackFrame(Word.StackFrameCommand.CONTINUE, _a, _b, _c, _d);
            });
        }
        
        //newline
        {
            addDefaultWord(DefaultTypes.ANY, "\\n", DefaultTypes.ANY, _dictionary, (_deque, _a, _b, _c, _d) ->
            {
                System.out.println();
                return new Word.StackFrame(Word.StackFrameCommand.CONTINUE, _a, _b, _c, _d);
            });
        }
    }
    
    private static String[] toLines(final String _source)
    {
        return _source.split(";");
    }
    
    private static String[] splitHeaderFromDefinition(final String _line)
    {
        final String[] out = new String[2];
        
        for (int i = 0; i < _line.length(); i++)
        {
            if (_line.substring(i, i + 1).equals(":"))
            {
                out[0] = _line.substring(0, i).trim();
                out[1] = _line.substring(i + 1).trim();
                return out;
            }
        }
        
        throw new RuntimeException("unexpected line: " + _line);
    }
    
    private static List<Token> tokenize(final String _line)
    {
        final Spigot spigot = new Spigot(_line);
        
        final StringBuilder builder = new StringBuilder();
        final List<Token> tokens = new ArrayList<>();
        
        while (spigot.hasNext())
        {
            final String currentSelection = spigot.next();
            
            //white space
            if (currentSelection.matches("\\s"))
            {
                if (builder.length() != 0)
                {
                    tokens.add(new Token(builder.toString()));
                    builder.setLength(0);
                }
                continue;
            }
            
            //scope
            if (currentSelection.equals("{"))
            {
                if (builder.length() != 0)
                {
                    tokens.add(new Token(builder.toString()));
                    builder.setLength(0);
                }
                
                builder.append(currentSelection);
                
                int count = 0;
                
                while (spigot.hasNext())
                {
                    final String scopeSelection = spigot.next();
                    
                    builder.append(scopeSelection);
                    
                    if (scopeSelection.equals("{"))
                    {
                        count++;
                        continue;
                    }
                    
                    if (scopeSelection.equals("}"))
                    {
                        if (count == 0)
                        {
                            break;
                        }
                        count--;
                    }
                }
                
                tokens.add(new Token(builder.toString()));
                builder.setLength(0);
                
                continue;
            }
            
            //quote
            if (currentSelection.equals("\""))
            {
                if (builder.length() != 0)
                {
                    tokens.add(new Token(builder.toString()));
                    builder.setLength(0);
                }
                
                builder.append(currentSelection);
                
                while (spigot.hasNext())
                {
                    String quoteSelection = spigot.next();
                    
                    if (quoteSelection.equals("\\"))
                    {
                        if (spigot.hasNext())
                        {
                            quoteSelection += spigot.next();
                        }
                    }
                    
                    switch (quoteSelection)
                    {
                        case "\\n":
                            builder.append("\n");
                            continue;
                        
                        case "\\\"":
                            builder.append("\"");
                            break;
                        
                        case "\\\\":
                            builder.append("\\");
                            break;
                        
                        default:
                            builder.append(quoteSelection);
                    }
                    
                    if (quoteSelection.equals("\""))
                    {
                        break;
                    }
                }
                
                tokens.add(new Token(builder.toString()));
                builder.setLength(0);
                
                continue;
            }
            
            builder.append(currentSelection);
        }
        
        if (builder.length() != 0)
        {
            tokens.add(new Token(builder.toString()));
        }
        
        return tokens;
    }
    
    private static Word buildWord(final Header _header, final String _definition, final Map<Token, Word> _dictionary)
    {
        final List<Token> tokens = tokenize(_definition);
        final Word[] definition = new Word[tokens.size()];
        
        int anonymousWords = -1;
        
        for (int i = 0; i < definition.length; i++)
        {
            final Token wordName = tokens.get(i);
            
            Word word = _dictionary.get(wordName);
            
            if (word == null)
            {
                switch (wordName.type)
                {
                    case SCOPE:
                        anonymousWords++;
                        final Token anonymousName = new Token(_header.name + "λ" + anonymousWords);
                        
                        if (_dictionary.containsKey(anonymousName))
                        {
                            throw new RuntimeException("magical duplicate anonymous word: " + anonymousName);
                        }
                        
                        final Header anonymousHeader = new Header(DefaultTypes.ANY_TOKEN, anonymousName, DefaultTypes.UNKNOWN_TOKEN);
                        word = buildWord(anonymousHeader, wordName.nameNoEdges, _dictionary);
                        _dictionary.put(anonymousName, word);
                        break;
                    
                    case INBOUND_ALIAS:
                        word = new Word(new Header(wordName.nameNoEdges, wordName.name, wordName.nameNoEdges),
                                (_deque, _a, _b, _c, _d) -> new Word.StackFrame(Word.StackFrameCommand.CONTINUE, _a, _b, _c, _d));
                        _dictionary.put(wordName, word);
                        break;
                    
                    case OUTBOUND_ALIAS:
                        word = new Word(new Header(DefaultTypes.ANY, wordName.name, wordName.nameNoEdges),
                                (_deque, _a, _b, _c, _d) -> new Word.StackFrame(Word.StackFrameCommand.CONTINUE, _a, _b, _c, _d));
                        _dictionary.put(wordName, word);
                        break;
                    
                    case PRINTABLE:
                        word = new Word(new Header(DefaultTypes.ANY_TOKEN, wordName, DefaultTypes.ANY_TOKEN), (_deque, _a, _b, _c, _d) ->
                        {
                            System.out.print(wordName.nameNoEdges);
                            return new Word.StackFrame(Word.StackFrameCommand.CONTINUE, _a, _b, _c, _d);
                        });
                        _dictionary.put(wordName, word);
                        break;
                    
                    case DECIMAL:
                        final int decimal = Integer.parseInt(wordName.name);
                        word = new Word(new Header(DefaultTypes.ANY_TOKEN, wordName, DefaultTypes.INT_TOKEN), (_deque, _a, _b, _c, _d) ->
                        {
                            _deque.push(decimal);
                            return new Word.StackFrame(Word.StackFrameCommand.CONTINUE, _a, _b, _c, _d);
                        });
                        _dictionary.put(wordName, word);
                        break;
                    
                    default:
                        throw new RuntimeException("unknown word: " + wordName);
                }
            }
            
            definition[i] = word;
        }
        
        return new Word(_header, definition);
    }
}
