public class DefaultTypes
{
    public static final String INT = "int";
    public static final Token INT_TOKEN = new Token(INT);
    
    public static final String BOOL = "bool";
    public static final Token BOOL_TOKEN = new Token(BOOL);
    public static final int TRUE = -1;
    public static final int FALSE = 0;

    public static final String UNKNOWN = "unknown";
    public static final Token UNKNOWN_TOKEN = new Token(UNKNOWN);

    public static final String ANY = "any";
    public static final Token ANY_TOKEN = new Token(ANY);
    
    public static final String STORE_A = "store_a";
    public static final Token STORE_A_TOKEN = new Token(STORE_A);
    
    public static final String STORE_B = "store_b";
    public static final Token STORE_B_TOKEN = new Token(STORE_B);
    
    public static final String STORE_C = "store_c";
    public static final Token STORE_C_TOKEN = new Token(STORE_C);
    
    public static final String STORE_D = "store_d";
    public static final Token STORE_D_TOKEN = new Token(STORE_D);
}
