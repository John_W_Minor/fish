public class Token
{
    public final String name;
    public final String nameNoEdges;
    public final TokenType type;

    public Token(final String _name)
    {
        name = _name;
        type = TokenType.match(name);

        if (type == TokenType.SCOPE || type == TokenType.INBOUND_ALIAS || type == TokenType.OUTBOUND_ALIAS || type == TokenType.PRINTABLE)
        {
            nameNoEdges = name.substring(1, name.length() - 1);
        } else
        {
            nameNoEdges = name;
        }
    }

    @Override
    public String toString()
    {
        return name;
    }

    @Override
    public boolean equals(final Object _o)
    {
        if (this == _o)
        {
            return true;
        }

        if (!(_o instanceof Token))
        {
            return false;
        }

        final Token other = (Token) _o;

        return name.equals(other.name);
    }

    @Override
    public int hashCode()
    {
        return name.hashCode();
    }

    public enum TokenType
    {
        SCOPE("\\{(\\s|\\S)*\\}"),
        INBOUND_ALIAS("<\\S*<"),
        OUTBOUND_ALIAS(">\\S*>"),
        PRINTABLE("\"(\\s|\\S)*\""),
        DECIMAL("-?[0-9]+"),
        ANY("\\S*");

        public final String regex;

        TokenType(final String _regex)
        {
            regex = _regex;
        }

        public static TokenType match(final String _name)
        {
            if (_name.matches(SCOPE.regex))
            {
                return SCOPE;
            }

            if(_name.matches(INBOUND_ALIAS.regex))
            {
                return INBOUND_ALIAS;
            }

            if(_name.matches(OUTBOUND_ALIAS.regex))
            {
                return OUTBOUND_ALIAS;
            }

            if (_name.matches(PRINTABLE.regex))
            {
                return PRINTABLE;
            }

            if (_name.matches(DECIMAL.regex))
            {
                return DECIMAL;
            }

            return ANY;
        }
    }
}
