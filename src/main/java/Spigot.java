public class Spigot
{
    private final String data;
    private int index = 0;

    public Spigot(final String _data)
    {
        data = _data;
    }

    public boolean hasNext()
    {
        return index < data.length();
    }

    public String next()
    {
        if(!hasNext())
        {
            return null;
        }

        final String out = data.substring(index, index + 1);
        index++;

        return out;
    }
}
