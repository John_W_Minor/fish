blank int:
    0;

x int:
    10;

oh int:
    100;

board copy_board board:
    9 duplicate;

board print_cell any:
    {
        1 duplicate blank ==
            if_true
                "- "
    }
    {
        1 duplicate x ==
            if_true
                "x "
    }
    {
        oh ==
            if_true
                "o "
    };

board print_board any:
    copy_board

    \n print_cell print_cell print_cell
    \n print_cell print_cell print_cell
    \n print_cell print_cell print_cell

    \n
    \n "1 2 3"
    \n "4 5 6"
    \n "7 8 9"

    \n
    \n "====="
    \n;

board:
    blank blank blank
    blank blank blank
    blank blank blank;

int move move:
    io;

move check_move bool:
    store_a

    put_a 9 <= put_a 0 > && store_b

    {
        put_b >bool>
            if_true
                put_a 1 + get_value blank == put_a swap
    }

    put_b >bool>
        if_false
            put_a put_b;

move place_piece bool:
    check_move store_a put_a

    {
        if_true
            >int> set_value true
    }

    put_a >bool>
        if_false
            pop pop false;

board player_x board:
    \n "x move: " x move place_piece

    if_false
        \n "illegal move" \n >board> print_board loop;

board player_oh board:
    \n "o move: " oh move place_piece

    if_false
        \n "illegal move" \n >board> print_board loop;

board single_player game_loop:
    player_x print_board

    loop;

board multi_player game_loop:
    player_x print_board

    player_oh print_board

    loop;

tic_tac_toe:
    {
        "one or two players? " io store_a put_a

        put_a 1 != put_a 2 != &&
            if_true
                pop loop
    } store_a

    board

    print_board

    {
        put_a 1 ==
            if_true
                >board> single_player
    }

    {
        put_a 2 ==
            if_true
                >board> multi_player
    };

main:
    tic_tac_toe;
